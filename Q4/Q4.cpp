#include <bits/stdc++.h>
using namespace std;



const int INF = INT_MAX;
vector<pair<int, int>> adj[10001];

void dijkstra(int s, vector<int> & d, vector<int> & p,int n) {

    d.assign(n, INF);
    p.assign(n, -1);

    d[s] = 0;
    using pii = pair<int, int>;
    priority_queue<pii, vector<pii>, greater<pii>> q;
    q.push({0, s});
    while (!q.empty()) {
        int v = q.top().second;
        int d_v = q.top().first;
        q.pop();
        if (d_v != d[v])
            continue;

        for (auto edge : adj[v]) {
            int to = edge.first;
            int len = edge.second;

            if (d[v] + len < d[to]) {
                d[to] = d[v] + len;
                p[to] = v;
                q.push({d[to], to});
            }
        }
    }
}



int Dynamic(int node , int count,int coin[],int arr[][10],int maxLength){
    int lenCol = (node - 1) * 2 + 1;
    int dynamic[count][lenCol];
    bool visit[count][lenCol][node];
    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < lenCol; ++j) {
            for (int k = 0; k < node; ++k) {
                visit[i][j][k] = false;
            }
        }
    }
    for (int i = 0; i < count; ++i) {
        int minZero = INT_MAX;
        int indexMinZero = -1;
        for (int j = 1; j < lenCol; ++j) {
            int num;
            if (j % 2 == 0) {
                num = 0;
            } else {
                num = j / 2 + 1;
            }
            if (i <= coin[0]) {
                if (coin[num] == i) {
                    dynamic[i][j] = 0;
                } else {
                    dynamic[i][j] = INT_MAX;
                }
                if (i == coin[0]) {
                    visit[i][j][0] = true;
                }

            } else {
                int m = INT_MAX;
                int indexM = -1;
                if (j % 2 == 0) {
                    if (dynamic[i][j - 1] == INT_MAX) {
                        dynamic[i][j] = INT_MAX;
                    } else {
                        dynamic[i][j] = dynamic[i][j - 1] + arr[j / 2][0];
                        for (int k = 0; k < 5; ++k) {
                            visit[i][j][k] = visit[i][j - 1][k];
                        }
                        int lastMinZero = minZero;
                        minZero = min(minZero, dynamic[i][j]);
                        if (lastMinZero != minZero) {
                            indexMinZero = j;

                        }
                    }
                } else {

                    for (int k = 0; k < lenCol; ++k) {
                        int numk;
                        if (k % 2 == 0) {
                            numk = 0;
                        } else {
                            numk = k / 2 + 1;
                        }
                        if (numk == 0 && k != 0) {
                            continue;
                        }
                        if (i >= coin[num] && k != j) {
                            if (visit[i - coin[num]][k][num] == false) {
                                if (dynamic[i - coin[num]][k] == INT_MAX) {

                                    if (coin[num] == i) {
                                        int lastM = m;
                                        m = min(arr[0][num], m);
                                        if (lastM != m)
                                            indexM = j;
                                    } else {
                                        m = min(m, INT_MAX);
                                    }
                                } else {
                                    int lastM = m;
                                    m = min(m, dynamic[i - coin[num]][k] + arr[num][numk]);
                                    if (lastM != m)
                                        indexM = k;

                                }
                            }
                        }
                    }
                    dynamic[i][j] = m;
                    if (m != INT_MAX) {
                        for (int k = 0; k < node; ++k) {
                            visit[i][j][k] = visit[i - coin[num]][indexM][k];
                        }
                        visit[i][j][num] = true;
                    }
                }

            }


        }
        if (i < coin[0]) {
            dynamic[i][0] = INT_MAX;
        } else if (i == coin[0]) {
            dynamic[i][0] = 0;
            visit[i][0][0] = true;
        } else {
            dynamic[i][0] = minZero;
            for (int k = 0; k < node; ++k) {
                visit[i][0][k] = visit[i][indexMinZero][k];
            }
        }
    }
    int out = 0;
    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < lenCol; ++j) {
            if(j == 0 && dynamic[i][0] <= maxLength){
                out = i;
            }
        }
    }
    return out;

}

int main() {
    int t;
    cin>> t;
    int arrOut[t];
    for (int ttt = 0; ttt < t; ++ttt) {
        int N, M;
        cin >> N >> M;
        for (int i = 0; i < M; ++i) {
            int a, b, len;
            cin >> a >> b >> len;
            adj[a].push_back(make_pair(b, len));
            adj[b].push_back(make_pair(a, len));
        }
        int count;
        set<int> coinindex;
        cin >> count;
        int coin2[N];
        for (int i = 0; i < N; ++i) {
            coin2[i] = 0;
        }
        for (int i = 0; i < count; ++i) {
            int a;
            cin >> a;
            coin2[a]++;
            coinindex.insert(a);
        }
        int maxLength;
        cin >> maxLength;
        coinindex.insert(0);
        int node = coinindex.size();
        int arr2[node][node];
        set<int> deletedcoins;
        int indexI = 0;
        for (auto i = coinindex.begin(); i != coinindex.end(); i++) {
            int s = *i;
            vector<int> d;
            vector<int> p;
            dijkstra(s,d,p,N);
            int dest1 = 0;
            for (auto k = coinindex.begin(); k != coinindex.end(); k++) {
                int dest = *k;
                if (s == 0 && d[dest] == INF) {
                    deletedcoins.insert(dest);
                }
                arr2[indexI][dest1] = d[dest];
                ++dest1;
            }
            ++indexI;
        }

        for (auto k = deletedcoins.begin(); k != deletedcoins.end(); k++) {
            int delcoin = *k;
            coinindex.erase(delcoin);
        }

        int lastNode = node;
        node = coinindex.size();
        int coin[node];
        int indC = 0;
        for (auto k = coinindex.begin(); k != coinindex.end(); k++) {
            coin[indC] = coin2[*k];
            ++indC;
        }
        int arr[node][10];
        int indexArrRow = 0;
        for (int i = 0; i < lastNode; i++) {
            if (arr2[0][i] != INF) {
                int indexArrCol = 0;
                for (int j = 0; j < lastNode; j++) {
                    if (arr2[0][j] != INF) {
                        arr[indexArrRow][indexArrCol] = arr2[i][j];
                        indexArrCol++;
                    }
                }
                indexArrRow++;
            }
        }
        count = 1;
        for (auto i = 0; i<node; i++) {
            count += coin[i];
        }
        arrOut[ttt] = Dynamic(node,count,coin,arr,maxLength);
        for (int i = 0; i < N; ++i) {
            adj[i].clear();
        }
        coinindex.clear();
        deletedcoins.clear();
    }
    for (int ttt = 0; ttt < t; ++ttt) {
        cout<<arrOut[ttt]<<endl;
    }
    return 0;

}

