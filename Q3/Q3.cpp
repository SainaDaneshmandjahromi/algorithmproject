#include <bits/stdc++.h>
using namespace std;


int graph[310][310][6]; // In to Out itself , out to In itself, In to Left Out, In to Right Out,In to Up Out,In to Down out

int graph2[310][310];
pair<pair<int,int>,int> parent[310][310][2]; // i , j node , In/Out
//vector<pair<int,int>>& parent;
int N,M;
int n,m;

vector<pair<int,int>> sAdj;

int bfs(int s, pair<int,int> t) {
    for (int i = 0;i<= N;i++){
        for (int j=0; j<= M; j++){
            parent[i][j][0] = make_pair(make_pair(-10,-10),0);
            parent[i][j][1] = make_pair(make_pair(-10,-10),0);
        }
    }

    queue<pair<int, int>> q;
    queue <pair<int,int>> cost;




    for(pair<int,int> next:sAdj) {
        q.push(next);
        cost.push({INT_MAX,0});
        parent[next.first][next.second][0] = make_pair(make_pair(-1,-1),1);
    }



    while (!q.empty()) {
        int curI = q.front().first;
        int curJ = q.front().second;
        int flow = cost.front().first;
        int IN_Out = cost.front().second;
       // cout<<curI-1<<" "<<curJ-1<<" - "<<IN_Out<<" Par --->  "<<parent[curI][curJ][IN_Out].first.first-1<< " "<<parent[curI][curJ][IN_Out].first.second-1<<" - "<<parent[curI][curJ][IN_Out].second<<endl;
        /*cout<<curI<<" "<<curJ<<"   ";
        cout<<flow<<"  "<<IN_Out<<endl;
        cout<<"Parent :"<<parent[curI][curJ][IN_Out].first<<" "<<parent[curI][curJ][IN_Out].second<<endl;*/

        //cout<<endl;
        q.pop();
        cost.pop();


        if(IN_Out == 0){ // In
            pair <int,int> No = {-10,-10};
            int m1 = min(graph[curI][curJ][0],flow);
            //cout<<"min In : "<<m1<<endl;
            if(m1 != 0 && parent[curI][curJ][1].first == No) {
                q.push({curI, curJ});
                cost.push({m1, 1});
                int ind = curI * M + curJ;
                parent[curI][curJ][1] = make_pair(make_pair(curI,curJ),0);
            }

            if(parent[curI][curJ+1][1].first == No && graph[curI][curJ][3] != 0){
                int new_flow = min(flow,graph[curI][curJ][3]);
                parent[curI][curJ+1][1] = make_pair(make_pair(curI,curJ),0);
                if(make_pair(curI,curJ+1) == t){
                    parent[curI][curJ+1][0] = make_pair(make_pair(curI,curJ),0);
                    return new_flow;
                }
                q.push({curI,curJ+1});
                cost.push({new_flow,1});

            }

            if(parent[curI][curJ-1][1].first == No && graph[curI][curJ][2] != 0){
                int new_flow = min(flow,graph[curI][curJ][2]);
                parent[curI][curJ-1][1] = make_pair(make_pair(curI,curJ),0);
                if(make_pair(curI,curJ-1) == t){
                    parent[curI][curJ-1][0] = make_pair(make_pair(curI,curJ),0);
                    return new_flow;
                }
                //cout<<curI-1<<" "<<curJ<<"  p : "<<parent[curI][curJ-1][1].first.first<<endl;
                q.push({curI,curJ-1});
                cost.push({new_flow,1});

            }

            if(parent[curI+1][curJ][1].first == No && graph[curI][curJ][5] != 0){
                int new_flow = min(flow,graph[curI][curJ][5]);
                parent[curI+1][curJ][1] = make_pair(make_pair(curI,curJ),0);
                if(make_pair(curI+1,curJ) == t){
                    parent[curI+1][curJ][0] = make_pair(make_pair(curI,curJ),0);
                    return new_flow;
                }
                q.push({curI+1,curJ});
                cost.push({new_flow,1});

            }

            if(parent[curI-1][curJ][1].first == No && graph[curI][curJ][4] != 0){
                int new_flow = min(flow,graph[curI][curJ][4]);
                parent[curI-1][curJ][1] = make_pair(make_pair(curI,curJ),0);
                if(make_pair(curI-1,curJ) == t){
                    parent[curI-1][curJ][0] = make_pair(make_pair(curI,curJ),0);
                    return new_flow;
                }
                q.push({curI-1,curJ});
                cost.push({new_flow,1});

            }


        }
        else if(IN_Out == 1){ // Out
            int indU = (curI-1)*M + curJ;
            int indD = (curI+1)*M + curJ;
            int indL = curI*M + curJ-3;
            int indR = curI*M + curJ+1;
            pair <int,int> No = {-10,-10};

            if(graph[curI][curJ][1] != 0 && parent[curI][curJ][0].first == No){ //In
                q.push({curI,curJ});
                cost.push({min(graph[curI][curJ][1],flow),0});
                parent[curI][curJ][0] = make_pair(make_pair(curI,curJ),1);
            }

            if(parent[curI][curJ+1][0].first == No && (graph[curI][curJ+1][0] + graph[curI][curJ+1][1])> 0){
                //new_flow = min(flow,graph[curI][curJ+1]);
                parent[curI][curJ+1][0] = make_pair(make_pair(curI,curJ),1);
                if(make_pair(curI,curJ+1) == t){
                    return flow;
                }
                q.push({curI,curJ+1});
                cost.push({flow,0});

            }

            if(parent[curI][curJ-1][0].first == No&& (graph[curI][curJ-1][0] + graph[curI][curJ-1][1]) > 0){
                //new_flow = min(flow,graph[curI][curJ-1]);
                parent[curI][curJ-1][0] = make_pair(make_pair(curI,curJ),1);
                if(make_pair(curI,curJ-1) == t){
                    return flow;
                }
                q.push({curI,curJ-1});
                cost.push({flow,0});

            }

            if(parent[curI+1][curJ][0].first == No && (graph[curI+1][curJ][0] + graph[curI+1][curJ][1])> 0){
                //new_flow = min(flow,graph[curI+1][curJ]);
                parent[curI+1][curJ][0] = make_pair(make_pair(curI,curJ),1);
                if(make_pair(curI+1,curJ) == t){
                    return flow;
                }
                q.push({curI+1,curJ});
                cost.push({flow,0});

            }

            if(parent[curI-1][curJ][0].first == No&& (graph[curI-1][curJ][0] + graph[curI-1][curJ][1])> 0){
                //new_flow = min(flow,graph[curI-1][curJ][IN_Out]);
                parent[curI-1][curJ][0] = make_pair(make_pair(curI,curJ),1);
                if(make_pair(curI-1,curJ) == t){
                    return flow;
                }
                q.push({curI-1,curJ});
                cost.push({flow,0});

            }

        }
        else {

            /*for (int next : graph[]) {
                if (parent[next] == -1 && capacity[cur][next]) {
                    parent[next] = cur;
                    int new_flow = min(flow, capacity[cur][next]);
                    if (next == t)
                        return new_flow;
                    q.push({next, new_flow});
                }
            }*/
        }
    }

    return 0;
}

bool visit[310][310];
set<pair<int,int>> setBlock;

void dfs(int i,int j) {
    cout<<i<<"  "<<j<<endl;
    if (i == n && j == m) {
        cout << "badbakht shodiiiiiiiiiiiiiiiiiim" << endl;
        cout << endl;
        cout << endl;
    }
    if(i == -1 && j == -1){
        for(pair<int,int> next:sAdj) {
            visit[next.first][next.second] = true;
            dfs(next.first,next.second);
        }
    }

    if (graph[i - 1][j][0] > 0 && visit[i - 1][j] == false) {
        visit[i - 1][j] = true;
        dfs(i - 1, j);
    }
    else if(graph[i - 1][j][0] == 0 && visit[i - 1][j] == false && i-1 != 0 && j!= 0){
        if(graph2[i-1][j] != 0)
            setBlock.insert(make_pair(i-1,j));

    }

    if (graph[i + 1][j][0] > 0 && visit[i + 1][j] == false) {
        visit[i + 1][j] = true;
        dfs(i + 1, j);
    }
    else if(graph[i + 1][j][0] == 0 && visit[i + 1][j] == false && i+1 != 0 && j!= 0){
        if(graph2[i+1][j] != 0)
            setBlock.insert(make_pair(i+1,j));
    }

    if (graph[i][j - 1][0] > 0 && visit[i][j - 1] == false) {
        visit[i][j - 1] = true;
        dfs(i, j - 1);
    }
    else if(graph[i][j-1][0] == 0 && visit[i][j-1] == false && i != 0 && j-1!= 0){
        if(graph2[i][j-1] != 0)
            setBlock.insert(make_pair(i,j-1));
    }

    if (graph[i][j + 1][0] > 0 && visit[i][j + 1] == false) {
        visit[i][j + 1] = true;
        dfs(i, j + 1);
    }
    else if(graph[i ][j+1][0] == 0 && visit[i][j+1] == false && i != 0 && j+1 != 0){
        if(graph2[i][j+1] != 0)
            setBlock.insert(make_pair(i,j+1));
    }





}


int maxflow(int s, pair<int,int> t) {
    int flow = 0;
    //vector<pair<int,int>> parent((N+1)*(M+1)+1);
    int new_flow;

    while (new_flow = bfs(s, t)) {
        //cout << "floooooooow  " << new_flow << endl;

//        for (int i = 1; i <= N ; ++i) {
//            for (int j = 1; j <= M; ++j) {
//                cout<<graph[i][j][0]<<"  "<<graph[i][j][1]<<"     ";
//            }
//            cout<<endl;
//        }
        flow += new_flow;
        int curI = t.first;
        int curJ = t.second;

        /*for (int i = 1; i <= N; ++i) {
            for (int j = 1; j <= M; ++j) {

                cout<<parent[i][j][0].first<<"  "<<parent[i][j][0].second<<"     ";
                cout<<parent[i][j][1].first<<"  "<<parent[i][j][1].second<<"                 ";
            }
            cout<<endl;
        }*/
        //cout << "path__________" << endl;
        int prevInOut = 0;
        while (curI != s || curJ != s) {
            pair<int, int> cur = make_pair(curI, curJ);
            pair<int, int> prev;

            if (cur == t) {
                prev = parent[curI][curJ][0].first;
                prevInOut = parent[curI][curJ][0].second;
            } else {
                prev = parent[curI][curJ][prevInOut].first;
                prevInOut = parent[curI][curJ][prevInOut].second;
            }

//            if(cur == prev){
//                prev = parent[curI][curJ][0];
//            }
//            cout << curI - 1 << " " << curJ - 1 << "  parent -> " << prev.first - 1 << " " << prev.second - 1 << " I/O "
//                 << prevInOut << endl;


            //curI = prev.first;
            //curJ = prev.second;
            //cout<<curI<<curJ;
            if (curI == prev.first && curJ == prev.second && curI > 0 && curJ > 0) {
                //break;
                //if(graph[curI][curJ] != )
//                cout<<"last"<<endl;
//                cout<<
//
//                cout<<"endl"<<endl;
                int notPrevInOut;
                if(prevInOut == 1)
                    notPrevInOut = 0;
                else
                    notPrevInOut = 1;
                graph[curI][curJ][prevInOut] -= new_flow;
                graph[curI][curJ][notPrevInOut] += new_flow;

            }
            else if(prev.first != s){
                if(prevInOut == 1){
                    //cout<<" cur I,CurJ "<<curI-1 <<"  "<<curJ-1 <<" change khodesh -> "<<prev.first-1<<" "<<prev.second-1<<" I/o  "<<prevInOut<<endl;

                    if (curI - 1 == prev.first && curJ == prev.second) {
                        //cout<<"Up"<<endl;
                        graph[curI][curJ][4] += new_flow;
                    } else if (curI + 1 == prev.first && curJ == prev.second) {
                        //cout<<"Down"<<endl;
                        graph[curI][curJ][5] += new_flow;
                    } else if (curI == prev.first&& curJ + 1 == prev.second) {
                        //cout<<"Right"<<endl;
                        graph[curI][curJ][3] += new_flow;
                    } else if (curI == prev.first&& curJ - 1 == prev.second) {
                        //cout<<"left"<<endl;
                        graph[curI][curJ][2] += new_flow;
                    }
                }
                else{
                    //cout<<" cur I,CurJ "<<curI-1 <<"  "<<curJ-1 <<" change Par -> "<<prev.first-1<<" "<<prev.second-1<<" I/o  "<<prevInOut<<endl;

                    if (curI - 1 == prev.first&& curJ == prev.second) {
                        graph[prev.first][prev.second][5] -= new_flow;
                    } else if (curI + 1 == prev.first&& curJ == prev.second) {
                        graph[prev.first][prev.second][4] -= new_flow;
                    } else if (curI == prev.first&& curJ + 1 == prev.second) {
                        graph[prev.first][prev.second][2] -= new_flow;
                    } else if (curI == prev.first&& curJ - 1 == prev.second) {
                        graph[prev.first][prev.second][3] -= new_flow;
                    }
                }


            }
            curI = prev.first;
            curJ = prev.second;

        }
       // cout << "end__________________________" << endl;
        //break;



        /*cout<<"graph"<<endl;
        for (int i = 0; i <= N + 1; ++i) {
            for (int j = 0; j <= M + 1; ++j) {
                string s = to_string(graph[i][j][0]) + "_" + to_string(graph[i][j][1])+"_"+to_string(graph[i][j][2])+"_"+to_string(graph[i][j][3])+"_"+to_string(graph[i][j][4])+"_"+to_string(graph[i][j][5]);
                for (int k = s.size(); k < 15; k++) {
                    s.push_back(' ');
                }
                cout << s << " ";
            }
            cout << endl;
        }*/

    }

    return flow;
}






int main() {

    cin>>N>>M;



    for (int i = 0; i <= N; ++i) {
        graph[i][0][0] = 0;
        graph[i][0][1] = 0;
        graph[i][0][2] = 0;
        graph[i][0][3] = 0;
        graph[i][0][4] = 0;
        graph[i][0][5] = 0;
        graph[i][M+1][0] = 0;
        graph[i][M+1][1] = 0;
        graph[i][M+1][2] = 0;
        graph[i][M+1][3] = 0;
        graph[i][M+1][4] = 0;
        graph[i][M+1][5] = 0;
    }

    for (int j = 0; j <= M; ++j) {
        graph[0][j][0] = 0;
        graph[0][j][1] = 0;
        graph[0][j][2] = 0;
        graph[0][j][3] = 0;
        graph[0][j][4] = 0;
        graph[0][j][5] = 0;
        graph[N+1][j][0] = 0;
        graph[N+1][j][1] = 0;
        graph[N+1][j][2] = 0;
        graph[N+1][j][3] = 0;
        graph[N+1][j][4] = 0;
        graph[N+1][j][5] = 0;
    }

    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= M; ++j) {
            int a;
            cin>>a;
            if(i == 1 || j == 1 || i == N || j == M){
                if(a != 0) {
                    sAdj.push_back({i, j});
                }
            }
            graph[i][j][0] = a;
            graph[i][j][1] = 0;
            graph[i][j][2] = 0;
            graph[i][j][3] = 0;
            graph[i][j][4] = 0;
            graph[i][j][5] = 0;
            graph2[i][j] = a;
        }
    }
    cin>>n>>m;
    ++n;
    ++m;
    int max1 = maxflow( -1 , { n , m });
    int out = min(max1,graph[n][m][0]);
    cout<<out<<endl;
/*
    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= M ; ++j) {
            visit[i][j] = false;
        }
    }
    //visit[n][m] = true;
    dfs(-1,-1);
    int out1 = 0;
    for(auto i : setBlock){
        out1+= graph2[i.first][i.second];
        cout<<"block : "<<i.first<<" "<<i.second <<"weight : "<<graph2[i.first][i.second]<<endl;
    }
    cout<<out1;*/



    return 0;
}

/*
 4 4
10 1000 0 10000
1000 100 100 1000
10 1000 50 0
2000 5 10 1000
2 2
 */