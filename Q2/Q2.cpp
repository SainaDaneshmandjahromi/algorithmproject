#include <bits/stdc++.h>
using namespace std;



void bfs(int arr[1005][1005],queue<pair<int, int> > q,queue<int> qk,int K){

    while(q.size() > 0) {
        pair<int, int> u = q.front();
        q.pop();
        int k = qk.front();
        qk.pop();
        k+=K;
        int i = u.first;
        int j = u.second;

        if (arr[i + 1][j - 1] == 0) {
            arr[i + 1][j - 1] = k;
            q.push(make_pair(i + 1, j - 1));
            qk.push(k);
        }
        if (arr[i + 1][j] == 0) {
            arr[i + 1][j] = k;
            q.push(make_pair(i + 1, j));
            qk.push(k);
        }
        if (arr[i + 1][j + 1] == 0) {
            arr[i + 1][j + 1] = k;
            q.push(make_pair(i + 1, j + 1));
            qk.push(k);
        }
        if (arr[i][j - 1] == 0) {
            arr[i][j - 1] = k;
            q.push(make_pair(i, j - 1));
            qk.push(k);
        }
        if (arr[i][j + 1] == 0) {
            arr[i][j + 1] = k;
            q.push(make_pair(i, j + 1));
            qk.push(k);
        }
        if (arr[i - 1][j - 1] == 0) {
            arr[i - 1][j - 1] = k;
            q.push(make_pair(i - 1, j - 1));
            qk.push(k);
        }
        if (arr[i - 1][j] == 0) {
            arr[i - 1][j] = k;
            q.push(make_pair(i - 1, j));
            qk.push(k);
        }
        if (arr[i - 1][j + 1] == 0) {
            arr[i - 1][j + 1] = k;
            q.push(make_pair(i - 1, j + 1));
            qk.push(k);
        }

        /*for (int i = 0; i <= n+1; ++i) {
            for (int j = 0; j <= m+1; ++j) {
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;*/
    }

}


void bfs2(int arr[1005][1005],pair<int,int> s , pair<int,int> t){
    queue<pair<int, int> > q;
    queue<int> qk;
    q.push(s);
    qk.push(1);
    arr[s.first][s.second] = 1;

    while(q.size() > 0) {
        pair<int, int> u = q.front();
        q.pop();
        int k = qk.front();
        qk.pop();
        k++;
        int i = u.first;
        int j = u.second;


        if (arr[i + 1][j] > k && arr[i + 1][j]!= -1) {
            arr[i + 1][j] = k;
            q.push(make_pair(i + 1, j));
            qk.push(k);
            if(t.first == i+1 && t.second == j) {
                cout << k-1 << endl;
                return;
            }
        }
        else if (arr[i + 1][j] == 0 ){
            arr[i + 1][j] = k;
            q.push(make_pair(i + 1, j));
            qk.push(k);
            if(t.first == i+1 && t.second == j) {
                cout << k-1 << endl;
                return;
            }

        }


        if (arr[i][j - 1] > k && arr[i][j - 1]!= -1) {
            arr[i][j - 1] = k;
            q.push(make_pair(i, j - 1));
            qk.push(k);
            if(t.first == i && t.second == j-1) {
                cout << k-1 << endl;
                return;
            }
        }
        else if (arr[i][j - 1] == 0) {
            arr[i][j - 1] = k;
            q.push(make_pair(i, j - 1));
            qk.push(k);
            if(t.first == i && t.second == j-1) {
                cout << k-1 << endl;
                return;
            }
        }

        if (arr[i][j + 1] > k && arr[i][j + 1]!= -1) {
            arr[i][j + 1] = k;
            q.push(make_pair(i, j + 1));
            qk.push(k);
            if(t.first == i && t.second == j+1) {
                cout << k-1 << endl;
                return;
            }
        }
        else if (arr[i][j + 1] == 0 ){
            arr[i][j + 1] = k;
            q.push(make_pair(i, j + 1));
            qk.push(k);
            if(t.first == i && t.second == j+1) {
                cout << k-1 << endl;
                return;
            }
        }


        if (arr[i - 1][j] > k && arr[i - 1][j]!= -1) {
            arr[i - 1][j] = k;
            q.push(make_pair(i - 1, j));
            qk.push(k);
            if(t.first == i-1 && t.second == j) {
                cout << k-1 << endl;
                return;
            }
        }
        else if (arr[i - 1][j] == 0) {
            arr[i - 1][j] = k;
            q.push(make_pair(i - 1, j));
            qk.push(k);
            if(t.first == i-1 && t.second == j) {
                cout << k-1 << endl;
                return;
            }
        }


        /*for (int i = 0; i <= n+1; ++i) {
            for (int j = 0; j <= m+1; ++j) {
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;*/


    }
    cout<<"Impossible"<<endl;

}

int main() {
    bool b = true;

    while (b) {
        int n, m, k;
        cin >> n >> m >> k;
        if(n == 0 && m == 0 && k == 0){
            b = false;
            break;
        }
        queue<pair<int, int> > q;
        queue<int> qk;

        int arr[1005][1005];
        pair<int,int> s;
        pair<int,int> t;

        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                char c;
                cin >> c;
                if (c == 'f') {
                    arr[i][j] = 1;
                    q.push(make_pair(i, j));
                    qk.push(1);

                } else if (c == '-') {
                    arr[i][j] = 0;
                }else if(c == 's'){
                    arr[i][j] = 0;
                    s = make_pair(i,j);
                } else if(c == 't'){
                    arr[i][j] = 0;
                    t = make_pair(i,j);
                }
            }
        }
        for (int i = 0; i <= n+1 ; ++i) {
            arr[i][0] = -1;
            arr[i][m+1] = -1;
        }
        for (int j = 0; j <= m+1; ++j) {
            arr[0][j] = -1;
            arr[n+1][j] = -1;
        }

        bfs(arr,q,qk,k);

        /*for (int i = 0; i <= n+1 ; ++i) {
            for (int j = 0; j <= m+1 ; ++j) {
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }*/

        bfs2(arr,s,t);
        /*for (int i = 0; i <= n+1 ; ++i) {
            for (int j = 0; j <= m+1 ; ++j) {
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }*/



    }

    return 0;
}




