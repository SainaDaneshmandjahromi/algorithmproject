#include <bits/stdc++.h>
using namespace std;



void bfs(int arr[1005][1005],queue<pair<int, int> > q,queue<int> qk,int K){

    int iMin ;
    int jMin ;
    int lastk ;
    while(q.size() > 0) {
        pair<int, int> u = q.front();
        q.pop();
        int k = qk.front();
        qk.pop();
        k+=K;
        int i = u.first;
        int j = u.second;

        if(k != lastk){
            iMin = i;
            jMin = j;
        }
        else{
            if(i < iMin){
                iMin = i;
                jMin = j;
            }
            if(i == iMin){
                if(j < jMin)
                    jMin = j;
            }
        }
        if (arr[i + 1][j - 1] == 0) {
            arr[i + 1][j - 1] = k;
            q.push(make_pair(i + 1, j - 1));
            qk.push(k);
        }
        if (arr[i + 1][j] == 0) {
            arr[i + 1][j] = k;
            q.push(make_pair(i + 1, j));
            qk.push(k);
        }
        if (arr[i + 1][j + 1] == 0) {
            arr[i + 1][j + 1] = k;
            q.push(make_pair(i + 1, j + 1));
            qk.push(k);
        }
        if (arr[i][j - 1] == 0) {
            arr[i][j - 1] = k;
            q.push(make_pair(i, j - 1));
            qk.push(k);
        }
        if (arr[i][j + 1] == 0) {
            arr[i][j + 1] = k;
            q.push(make_pair(i, j + 1));
            qk.push(k);
        }
        if (arr[i - 1][j - 1] == 0) {
            arr[i - 1][j - 1] = k;
            q.push(make_pair(i - 1, j - 1));
            qk.push(k);
        }
        if (arr[i - 1][j] == 0) {
            arr[i - 1][j] = k;
            q.push(make_pair(i - 1, j));
            qk.push(k);
        }
        if (arr[i - 1][j + 1] == 0) {
            arr[i - 1][j + 1] = k;
            q.push(make_pair(i - 1, j + 1));
            qk.push(k);
        }

        /*for (int i = 0; i <= n+1; ++i) {
            for (int j = 0; j <= m+1; ++j) {
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;*/

        if (q.size() == 0) {
            cout<<iMin-1<<" "<<jMin-1<<endl;
            //cout<<endl;
        }
        lastk = k;
    }


}

int main() {
    bool b = true;

    while (b) {
        int n, m, k;
        cin >> n >> m >> k;
        if(n == 0 && m == 0 && k == 0){
            b = false;
            break;
        }
        queue<pair<int, int> > q;
        queue<int> qk;

        int arr[1005][1005];

        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= m; ++j) {
                char c;
                cin >> c;
                if (c == 'f') {
                    arr[i][j] = 1;
                    q.push(make_pair(i, j));
                    qk.push(1);

                } else if (c == '-') {
                    arr[i][j] = 0;
                }
            }
        }
        for (int i = 0; i <= n+1 ; ++i) {
            arr[i][0] = -1;
            arr[i][m+1] = -1;
        }
        for (int j = 0; j <= m+1; ++j) {
            arr[0][j] = -1;
            arr[n+1][j] = -1;
        }
        if(q.size() == 0){
            cout<<"0 0"<<endl;
        }
        else {
            bfs(arr, q, qk, k);
        }
        /*for (int i = 0; i <= n+1 ; ++i) {
            for (int j = 0; j <= m+1 ; ++j) {
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }*/


    }
    
    return 0;
}

